��    3      �  G   L      h  b   i  [   �  �   (          -     F     b     h     y  -  �     �     �     �     �     �                    #     2     A     F  �   M     �  �   		  �   �	  )   <
  ,   f
  *   �
     �
     �
  !   �
     �
     �
            
        %     *     /     >     Q     W  �   o  9        <     I     i     �  0   �     �  �   �  c   �  t  �     g  .   �     �     �  /   �  )     �  <  
   �     �     �  =        S  !   s     �     �  +   �     �     �           N   &  �   u  �   W  ?   5  C   u  C   �     �       5        T     g  !   v     �     �  
   �     �     �  %   �       ,     �   J  F   )     p  '   }  '   �  1   �  J   �        &      )           !             ,         (   .                                $      /           '                  0            *                       #      %   "              3                    1      
          2   +       -   	              You are executing a new version of pyLinuxWheel. Do you want to update Logitech wheel udev rules? <a href="https://www.jugandoenlinux.com"title="jugandoenlinux">www.jugandoenlinux.com</a>   <b>Credits</b>

Alberto Vicente aka <i>Odin</i>, lead developer
CansecoGPC, logo designer
Leillo1975, SQA
Francisco aka <i>P_Vader</i>, SQA
Krafting, french translation
Hüseyin Fahri Uzun, turkish translation
Harry Kane, russian translation <b>Preferences</b> <b>Special thanks to</b> <b>pyLinuxWheel</b>
v0.6.1  About Advanced options Alternate Modes An utility to configure Logitech steering
wheels  for Linux under the <a href="https://www.gnu.org/licenses/gpl-3.0.html" title="GPLv3 License">GPLv3 License</a>.
Go to <a href="https://gitlab.com/OdinTdh/pyLinuxWheel"                 title="Our website">pyLinuxWheel</a> website
for more information. Author Brake pedal Button clicked Check udev rules at start: Clutch pedal Combine Pedals Description Export Export profile Force Feedback Gain Import It is necessary an Internet connection to update logitech wheel udev rules. Do you want to update logitech wheel udev rules to execute pyLinuxWheel as normal user ? Load last saved value: Logitech wheel udev rules are not installed. You can not change your wheel driver without root privileges and with Logitech wheel udev rules non installed Logitech wheel udev rules are not update. You can not change your wheel driver without root privileges and with logitech wheel udev rules non installed Logitech wheel udev rules are not updated Logitech wheel udev rules has been installed Logitech wheel udev rules has been updated Name New Profile Please choose a profile to import Preferences Profile Profile Editor Range Resistance Tags Test Throttle pedal Update udev rules: Wheel Wheel Editor Properties You are executing pyLinuxWheel without root privileges. Do you want to install Logitech wheel udev rules to execute  pyLinuxWheel as normal user ? adjust the overall strength of the force feedback effects pyLinuxWheel pyLinuxWheel Post-Configuration pyLinuxWheel Pre-Configuration pyLinuxWheel Udev Configuration set the strength need to turn the steering wheel Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-03 14:10+0300
Last-Translator: 
Language-Team: 
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Plural-Forms: nplurals=2; plural=(n != 1);
  Вы запускаете новую версию pyLinuxWheel. Вы хотите обновить правила udev для рулей Logitech? <a href=“https://www.jugandoenlinux.com”title=“jugandoenlinux”>www.jugandoenlinux.com</a>   <b>Авторы</b>

Alberto Vicente aka <i>Odin</i>, главный разработчик
CansecoGPC, дизайнер логотипа
Leillo1975, SQA
Francisco aka <i>P_Vader</i>, SQA
Krafting, перевод на французский язык
Hüseyin Fahri Uzun, перевод на турецкий язык
Harry Kane, перевод на русский язык <b>Настройки</b> <b>Особая благодарность</b> <b>pyLinuxWheel</b>
v0.6.1  О программе Дополнительные настройки Альтернативные режимы Программа для настройки рулей Logitech
для Linux под <a href=“https://www.gnu.org/licenses/gpl-3.0.html” title=“GPLv3 License”>GPLv3 License</a>.
Перейдите на сайт <a href=“https://gitlab.com/OdinTdh/pyLinuxWheel”                 title=“Our website”>pyLinuxWheel</a> 
для получения дополнительной информации. Автор Педаль тормоза Кнопка нажата Проверять правила udev при запуске: Педаль сцепления Объединить педали Описание Экспорт Экспортировать профиль Force Feedback Усиление Импорт Для обновления правил udev рулей Logitech требуется интернет-соединение. Вы хотите обновить правил udev рулей Logitech и запустить pyLinuxWheel от обычного пользователя? Загрузить последнее сохраненное значение: Правила udev рулей Logitech не установлены. Вы не можете изменить настройки руля без прав root и установленных правил udev рулей Logitech Правила udev рулей Logitech не обновлены. Вы не можете изменить настройки руля без прав root и установленных правил udev рулей Logitech Правила udev рулей Logitech не обновлены Правила udev рулей Logitech не установлены Правила udev рулей Logitech были обновлены Имя Новый профиль Выберите профиль для импорта Настройки Профиль Редактор профилей Диапазон Сопротивление Метки Тест Педаль газа Обновить правила udev: Руль Свойства редактора руля Вы запускаете pyLinuxWheel без прав root. Вы хотите установить правила udev рулей Logitech и запустить pyLinuxWheel от обычного пользователя? применить общую силу эффектов force feedback pyLinuxWheel Постнастройка pyLinuxWheel Преднастройка pyLinuxWheel Настрйока правил udev pyLinuxWheel задать силу, требуемую для поворота руля 