# License of images

Jugandoenlinuxlog.png file, from https://jugandoenlinux.com/ , is under Attribution-ShareAlike 4.0 International 
(CC BY-SA 4.0) license and his original creator is Pascual M. Gómez

logo-pyLinuxWheel.png file, created by CansecoGPC. This file is under Attribution-ShareAlike 4.0 International 
(CC BY-SA 4.0) license. CansecoGPC has designed this image reusing the Stycil Tux icon from  Josh 'Cheeseness' Bush

banner.png and icons are derivated from logo-pyLinuxWheel.png file. These file are under Attribution-ShareAlike 4.0 
International (CC BY-SA 4.0) license

pylinuxhweel-screenshot.png is under Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license